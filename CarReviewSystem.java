import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class CarReviewSystem extends Application {

    private ListView<String> reviewListView = new ListView<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Car Review System");

        // Create a VBox for the main layout
        VBox vbox = new VBox(10);
        vbox.setPadding(new Insets(10));

        // Create labels and input fields for car review
        Label carNameLabel = new Label("Car Name:");
        TextField carNameField = new TextField();

        Label reviewerLabel = new Label("Reviewer Name:");
        TextField reviewerField = new TextField();

        Label reviewTextLabel = new Label("Review Text:");
        TextArea reviewTextArea = new TextArea();

        Button addButton = new Button("Add Review");
        addButton
                .setOnAction(e -> addReview(carNameField.getText(), reviewerField.getText(), reviewTextArea.getText()));

        // Add components to the VBox
        vbox.getChildren().addAll(
                carNameLabel, carNameField,
                reviewerLabel, reviewerField,
                reviewTextLabel, reviewTextArea,
                addButton, reviewListView);

        // Create a scene and set it to the stage
        Scene scene = new Scene(vbox, 400, 400);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private void addReview(String carName, String reviewerName, String reviewText) {
        String review = "Car: " + carName + "\nReviewer: " + reviewerName + "\nReview: " + reviewText;
        reviewListView.getItems().add(review);
    }
}
