import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    Stage primaryStage;
    Text title , inputHint;
    ImageView qrCode , mainImageView ;
    TextField input;
    Button download , create;

    Scene mainScene;

    String fileName;

     @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
    
        setUIComponents();

        Group root = setStackPane();

        mainScene = new Scene(root , 1000 , 800 , Color.LIGHTBLUE);

        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    private void setUIComponents(){

        primaryStage.setTitle("QR Code Genrator");
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("qr-code.png"));

        mainImageView = new ImageView( new Image("main_img.png"));
        mainImageView.setFitHeight(900);
        mainImageView.setFitWidth(1100);

        title = new Text("QR Code Genrator");
        title.setFill(Color.DARKRED);
        title.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 100));

        DropShadow shadow = new DropShadow();
        shadow.setRadius(8.0); // Adjust the shadow radius as needed
        shadow.setOffsetX(10.0); // Adjust the shadow offset on the X-axis
        shadow.setOffsetY(10.0); // Adjust the shadow offset on the Y-axis
        shadow.setColor(Color.GRAY); // Set the shadow color

        title.setEffect(shadow);

        inputHint = new Text("Enter the text, link, number here to Generate QR Code ....");
        inputHint.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));

        input = new TextField();
        input.setPrefSize(450, 30);

        create = new Button("Create QR Code");
        create.setPrefSize(180, 30);
        create.setStyle(
            "-fx-background-color: #4CAF50;" + // Background color
            "-fx-text-fill: white;" +  // Text color
            "-fx-font-weight: bold;" +  // Bold text
            "-fx-font-size: 20px;" +  // Font size
            "-fx-padding: 5 10 5 10;" +  // Padding (top, right, bottom, left)
            "-fx-border-radius: 5;" +  // Rounded corners
            "-fx-background-radius: 5;"  // Rounded background
        );

        DropShadow shadow2 = new DropShadow();
        shadow2.setRadius(8.0); // Adjust the shadow radius as needed
        shadow2.setOffsetX(5.0); // Adjust the shadow offset on the X-axis
        shadow2.setOffsetY(5.0); // Adjust the shadow offset on the Y-axis
        shadow2.setColor(Color.GREEN); // Set the shadow color

        create.setEffect(shadow2);

        qrCode = new ImageView(new Image("demo_scanner.png"));
        qrCode.setFitHeight(400);
        qrCode.setFitWidth(400);

        download = new Button("Download QR Code" , new ImageView(new Image("download.png")));
        download.setStyle(
            "-fx-background-color: gray;" + // Background color
            "-fx-text-fill: black;" +  // Text color
            "-fx-font-weight: bold;" +  // Bold text
            "-fx-font-size: 16px;" +  // Font size
            "-fx-padding: 5 10 5 10;" +  // Padding (top, right, bottom, left)
            "-fx-border-radius: 5;" +  // Rounded corners
            "-fx-background-radius: 5;"  // Rounded background
        );
        download.setVisible(false);

        
        create.setOnAction(e -> genrateQRCode());

        download.setOnAction(e -> downloadQRCode());


    }

    private Group setStackPane(){
        StackPane sp1 = new StackPane(mainImageView);
        sp1.setLayoutX(0);
        sp1.setLayoutY(0);

        StackPane sp2 = new StackPane(title);
        sp2.setLayoutX(120);
        sp2.setLayoutY(30);

        StackPane sp3 = new StackPane(inputHint);
        sp3.setLayoutX(220);
        sp3.setLayoutY(200);

        StackPane sp4 = new StackPane(input);
        sp4.setLayoutX(280);
        sp4.setLayoutY(230);

        StackPane sp5 = new StackPane(create);
        sp5.setLayoutX(400);
        sp5.setLayoutY(270);

        StackPane sp6 = new StackPane(qrCode);
        sp6.setLayoutX(300);
        sp6.setLayoutY(350);

        DropShadow shadow3 = new DropShadow();
        shadow3.setRadius(8.0); // Adjust the shadow radius as needed
        shadow3.setOffsetX(8.0); // Adjust the shadow offset on the X-axis
        shadow3.setOffsetY(8.0); // Adjust the shadow offset on the Y-axis
        shadow3.setColor(Color.RED); // Set the shadow color
        sp6.setEffect(shadow3);

        StackPane sp7 = new StackPane(download);
        sp7.setLayoutX(410);
        sp7.setLayoutY(760);


        return new Group( sp1 , sp2 , sp3 , sp4 , sp5 , sp6 , sp7);
    }

    private void popUp(String msg){

        primaryStage.hide();

        Stage stage = new Stage();
        stage.setTitle(" Error ");
        stage.getIcons().add(new Image("warning.png"));

        Text message = new Text(msg);
        message.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 25));
        
        Button ok = new Button("OK" , new ImageView(new Image("checked.png")));
        ok.setPrefSize(100, 20);
        
        
        StackPane sp1 = new StackPane(message);
        sp1.setLayoutX(20);
        sp1.setLayoutY(20);

        StackPane sp2 = new StackPane(ok);
        sp2.setLayoutX(200);
        sp2.setLayoutY(150);

        Group root = new Group(sp1 , sp2);

        Scene scene = new Scene(root , 500 , 200 , Color.ORANGERED);
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(e -> {
            stage.close();
            primaryStage.show();
        });

        ok.setOnAction(e ->{
            stage.close();
            primaryStage.show();
        });

    }

    private void genrateQRCode(){

        if(input.getText().equals("")){
                popUp("Please Enter the Valid Input !");
                return;
        }
        
        fileName = input.getText() + "-QR-Code.png";
        download.setVisible(true);
        String text = input.getText();

        try{

            BitMatrix bitMatrix = new QRCodeWriter().encode(text, BarcodeFormat.QR_CODE, 800 , 800);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", out);

            qrCode.setImage(new Image(new ByteArrayInputStream(out.toByteArray())));

        }catch(Exception e){
            popUp("Could Not Genrate QR Code !");
        }
    }

    private void downloadQRCode(){
        
        DirectoryChooser directoryChooser = new DirectoryChooser();

        File directoryToSave = directoryChooser.showDialog(primaryStage);

        if(directoryToSave == null){
            popUp("Please Select Valid Directory");
            return;
        }


        BufferedImage bufferedImage = SwingFXUtils.fromFXImage( qrCode.getImage() , null);
       
        File outputFile = new File(directoryToSave , fileName);

        try{
            // Write the BufferedImage to the file
            ImageIO.write(bufferedImage, "png", outputFile);

            popUp(fileName + " Saved");

        }catch(Exception e){
            e.printStackTrace();
            popUp("File Cannot Saved");
        }
    }

}